# Material Icons

## Présentation
Material Icons are delightful, beautifully crafted symbols for common actions and items. Download on desktop to use them in your digital products for Android, iOS, and web.

[![Material Icons](https://gitlab.com/bazooka/material-icons/wikis/uploads/7fed3843798e78d2dd734fed6df28290/Capture_d_e%CC%81cran_2018-12-27_a%CC%80_14.55.12.png)](https://material.io/tools/icons)

## Utilisation

Pour intégrer une icone dans du CSS, il suffit de ```@include materialIcons('help');```

## Installation
Pour importer le composant dans votre projet, il vous faut au préalable ```node```.

```npm i git+ssh://git@gitlab.com:bazooka/material-icons```

Puis importer les blockquotes dans votre scss avec : ```@import "node_modules/@bazooka/material-icons/material-icons"```;

## Développement
Le développement se fait sur la branche ```develop```.

## Changelog
Le format du changelog est calqué sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
tout en suivant la norme [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

### [1.0.0] - 27/12/2018
#### Added
- Material Icons Font
- Initial materialIcons mixin